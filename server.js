//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3001;

var bodyParse =require('body-parser');
app.use(bodyParse.json());
app.use(function(req,res,next){
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With,Content-Type, Accept");
  next();
});

var requestjson=require('request-json');


var path = require('path');

const db = require("./db");
const dbName = "oficinapolicia";
const collectionName = "denuncias";

db.initialize(dbName, collectionName, function (dbCollection) { // successCallback
    // lista todas las denuncias
    dbCollection.find().toArray(function (err, result) {
       if (err) throw err;
       console.log(result);
 
       // << return response to client >>
    });
 
    // << db CRUD routes >>
    app.post("/denuncia", (request, response) => {
       const denuncia = request.body;
       var datetime = new Date();
       denuncia.fecha=datetime;
       dbCollection.insertOne(denuncia, (error, result) => { // callback of insertOne
          if (error) throw error;
          // return updated list
          dbCollection.find().toArray((_error, _result) => { // callback of find
             if (_error) throw _error;
             response.json(_result);
          });
       });
    });
 
    app.get("/denuncias/:dni", (request, response) => {
       const itemId = request.params.dni;
 
       dbCollection.findOne({ dni: itemId }, (error, result) => {
          if (error) throw error;
          // return item
          response.json(result);
       });
    });
 
    app.get("/denuncias", (request, response) => {
       // return updated list
       dbCollection.find().toArray((error, result) => {
          if (error) throw error;
          response.json(result);
       });
    });
 
 
 }, function (err) { // failureCallback
    throw (err);
 });
 

 var urlOficinas="https://patrullajebicentenario.mininter.gob.pe/public/public/index/json_comisaria";
 var oficinaAPI=requestjson.createClient(urlOficinas);
 process.env['NODE_TLS_REJECT_UNAUTHORIZED'] = '0';
 app.get('/oficinas',function(req,res){
   oficinaAPI.get('',function(err,resM,body){
      body.cantidad=0;
     if(err){
      body.estado=err;
       console.log(err);
     }else{
      body.estado="ok";
      body.cantidad=body.comisarias.length;
      res.send(body);
     }
   });
 });


 app.get("/",function(req,res){
    res.sendFile(path.join(__dirname,'index.html'));
  })

 app.listen(port, () => {
    console.log(`Servidor ejecutando en puerto ${port}`);
 });
